# DEPRECATED

use [aytacworld-angular-overlay](https://www.npmjs.com/package/aytacworld-angular-overlay) instead.

# angular-simple-overlay

A simple overlay directive for Angular.

## Installation
Add the package to your application.

```
npm install --save angular-simple-overlay
```

## Getting started
Import the overlay module to your angular module.
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { OverlayModule } from 'ng-simple-overlay';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    OverlayModule
  ],
  declarations: [AppComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
```

Add the overlay directive to an element, like a container div.
```
<div [overlay]="true">...</div>
```

## Todo

- add text
- add spinner
- add custom text
- add custom style