export default {
  dest: 'dist/bundles/overlay.umd.js',
  entry: 'dist/index.js',
  format: 'umd',
  globals: {
    '@angular/core': 'ng.core',
  },
  moduleName: 'ng.overlay',
  sourceMap: false,
  external: ['@angular/core']
}